FROM gradle:jdk8-alpine

MAINTAINER FearNixx Technik, <technik@fearnixx.de>

USER root

RUN apk update \
	&& apk upgrade \
	&& apk add --no-cache --update curl ca-certificates openssl git tar unzip bash

USER gradle